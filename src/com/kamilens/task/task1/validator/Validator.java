package com.kamilens.task.task1.validator;

public abstract class Validator {
    /**
     * 1 ≤ n ≤ 10^5
     * */
    abstract void validateN();

    /**
    * 1≤ k ≤ n
    * */
    abstract void validateK(int k);

    /**
     * 1 ≤ cars[i] ≤ 10^14
     * */
    abstract void validateI();

    /**
     * All spots taken by cars are unique
     * */
    abstract void validateUnique();

    public final void validate(int[] cars, int k) {
        validateN();
        validateK(k);
        validateI();
        validateUnique();
    }
}
