package com.kamilens.task.task1;

import com.kamilens.task.task1.validator.ValidatorImpl;

import java.util.Arrays;

public class Task1 {

    /**
     * @param cars the parking spots where cars are parked
     * @param k    the number of cars that have to be covered by the roof
     * @return the minimum length of a roof that can cover k cars
     */
    public static int carParkingRoof(int[] cars, int k) {
        if (cars.length == 0 || k < 0) {
            return 0;
        }

        Arrays.sort(cars);

        int minDist = Integer.MAX_VALUE;

        for (int i = 0; i <= cars.length - k; i++) {
            minDist = Math.min(minDist, cars[i + k - 1] - cars[i]);
        }

        return minDist + 1;
    }
}
