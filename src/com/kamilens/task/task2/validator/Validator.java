package com.kamilens.task.task2.validator;

public abstract class Validator {
    /**
     * N and M are integers within the range [1...300,000]
     * */
    abstract void validateNAndMAreIntegersWithinTheRange(int[][] A);

    /**
    * the number of elements in matrix A is within the range [1...300,000];
    * */
    abstract void validateNumberOfElementsInMatrixAIsWithinTheRange(int[][] A);

    /**
    * each element of matrix A is an integers within  the range [-1,000,000,000...1,000,000,000]
    * */
    abstract void validateEachElementOfMatrixAIsAnIntegersWithinTheRange(int[][] A);

    public final void validate(int[][] A) {
        validateNAndMAreIntegersWithinTheRange(A);
        validateNumberOfElementsInMatrixAIsWithinTheRange(A);
        validateEachElementOfMatrixAIsAnIntegersWithinTheRange(A);
    }
}
