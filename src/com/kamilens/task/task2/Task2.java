package com.kamilens.task.task2;

public class Task2 {

    /**
     * @param A map
     * @return the number of different countries
     */
    public static int solve(int[][] A) {
        int rows = A.length;
        int cols = A[0].length;
        var map = initializeMap(rows, cols);
        var count = 0;

        for (int x = 0; x < A.length; x++) {
            for (int y = 0; y < A[0].length; y++) {
                if (!map[x][y]) {
                    continue;
                }
                count += 1;
                check(A[x][y], map, A, x, y);
            }
        }
        return count;
    }

    private static boolean[][] initializeMap(int rows, int cols) {
        var arr = new boolean[rows][cols];

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                arr[r][c] = true;
            }
        }

        return arr;
    }

    private static void check(int val, boolean[][] B, int[][] A, int x, int y) {
        if (x < 0 || x >= A.length) {
            return;
        }
        if (y < 0 || y >= A[0].length) {
            return;
        }
        if (!B[x][y]) {
            return;
        }
        if (A[x][y] != val) {
            return;
        }

        B[x][y] = false;
        check(val, B, A, x + 1, y);
        check(val, B, A, x - 1, y);
        check(val, B, A, x, y - 1);
        check(val, B, A, x, y + 1);
    }
}
