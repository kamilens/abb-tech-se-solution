package com.kamilens;

import com.kamilens.task.task1.Task1;
import com.kamilens.task.task2.Task2;

public class Main {

    public static void main(String[] args) {
        // Task 1
        var cars = new int[]{6, 2, 12, 7};
        var task1Result = Task1.carParkingRoof(cars, 3);
        System.out.printf("Task 1 result: %s (should be 6)\n", task1Result);

        // Task 2
        var map = new int[][] {
                {5, 4, 4},
                {4, 3, 4},
                {3, 2, 4},
                {2, 2, 2},
                {3, 3, 4},
                {1, 4, 4},
                {4, 1, 1}
        };
        var task2Result = Task2.solve(map);
        System.out.printf("Task 2 result: %s (should be 11)\n", task2Result);
    }
}
